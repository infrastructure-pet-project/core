# frozen_string_literal: true

RSpec.describe API::UsersController do
  it { expect(get('/api/users')).to eq(200) }
end
