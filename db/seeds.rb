# frozen_string_literal: true

User.find_or_create_by!(name: 'John')
User.find_or_create_by!(name: 'Sarah')
User.find_or_create_by!(name: 'Meg')
User.find_or_create_by!(name: 'Caren')
