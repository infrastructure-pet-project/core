# frozen_string_literal: true

module API
  class UsersController < ApplicationController
    def index
      render json: { users: User.all }
    end

    def show
      render json: { user: User.find(params[:id]) }
    end

    def create
      User.create!(permitted_attributes)
    end

    def update
      User.find(params[:id]).update!(permitted_attributes)
    end

    private

    def permitted_attributes
      params.require(:user).permit(:name)
    end
  end
end
