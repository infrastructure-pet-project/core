# frozen_string_literal: true

module API
  class InfosController < ApplicationController
    def show
      render json: {
        git_commit_sha: ENV.fetch('GIT_COMMIT_SHA', nil)
      }
    end
  end
end
