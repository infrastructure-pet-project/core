ARG RUBY_REPO=registry-service.container-registry:5000/ruby:3.0.4

FROM $RUBY_REPO as base
RUN gem install bundler:2.4.21
WORKDIR /app
COPY ./Gemfile ./Gemfile.lock ./
RUN bundle install

FROM base as production
COPY ./ .

FROM base as development