# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    resource :info, only: [:show]
    resources :users, only: %i[index show create update]
  end
end
